files=$@

lat=`pbpaste | awk -F', ' '{print $1}'`
lon=`pbpaste | awk -F', ' '{print $2}'`

if [ "$(echo "${lat} > 0" | bc)" -eq 1 ]
then
	latRef="N"
else
	latRef="S"
fi

if [ "$(echo "${lon} > 0" | bc)" -eq 1 ]
then
	lonRef="E"
else
	lonRef="W"
fi

echo $latRef $lonRef

/usr/local/bin/exiftool -GPSLatitude=$lat -GPSLatitudeRef=$latRef -GPSLongitude=$lon -GPSLongitudeRef=$lonRef $files