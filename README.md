# SetGPSTag

Get Latitude and Longitude from pasteboard, and set to files

## Requirement
* `exiftool` - http://owl.phy.queensu.ca/~phil/exiftool/

* `pbpaste` - macOS, most Linux OS have the command.

* Copy latitude and longitude to pasteboard - The format like `41.947423, -87.656323`

You can get latitude and longitude from some map services.

ex.) OpenStreetMap

1. `Secondary click` arbitary point on map, and select `Show address`
2. `Select` and `Copy`

![Map](Map.png)

# Usage
## on CLI
`setGPSTag.sh imagefile1 [imageFile2...]`

## as a Quick Action on macOS
1. Secondary click image file(s)
2. Select SetGPSTag

![Menu](Menu.png)

### How to make a Quick Action
1. Open 'Automator.app'
2. Choose 'Quick Action'
3. Add 'Run Shell Script' action
4. Paste 'SetGPSTag.sh'

then save

![Quick Action](QuickAction.png)
